Introduction
============

This bundle allows you to create your own symlinks/hardlinks/copies during assets:install.

This can be useful when using composer packages that were not specifically made for Symfony.


Installation / Usage
============

Execute: 

```
composer require lowentry/symfony-customizable-assets-install
```

In your app/AppKernel.php, add the bundle: 

```
$bundles = [
    ...
    new LowEntryCustomizableAssetsInstallBundle\LowEntryCustomizableAssetsInstallBundle()
];
```

In src/AppBundle/EventListener, create your event listener:

```
<?php
namespace AppBundle\EventListener;
class CustomizableAssetsInstallListener
{
    public function listen(\LowEntryCustomizableAssetsInstallBundle\Event\CustomizableAssetsInstallEvent $event)
    {
        // example:
        $event->makeLink($event->getVendorDir() . 'almasaeed2010/adminlte/', $event->getBundlesWebDir() . 'adminlte');
    }
}
```

In your app/config/services.yml, add the event listener:

```
    lowentry_customizable_assets_install_listener:
        class: AppBundle\EventListener\CustomizableAssetsInstallListener
        tags:
            - { name: kernel.event_listener, event: lowentry.customizable_assets_install, method: listen }
```

Done!

You should now be able to add custom links (symlinks/hardlinks/copies) in your event listener.


Notes
============

Note that this bundle was made for Symfony 3.1. It will likely work on other versions of Symfony as well, but that's not guaranteed.

Also note that you probably shouldn't use a slash ("/" or "\\") right after $event->getSomeDir(), because if the getSomeDir() function returns an empty string, the slash would (on Mac and Linux) cause it to start from the root directory (example: "/something"), whereas without a slash it will be a relative path. The $event->getSomeDir() will always end with a slash so an additional slash is not needed.
