<?php

namespace LowEntryCustomizableAssetsInstallBundle\Event;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;


class CustomizableAssetsInstallEvent extends Event
{
    const ID = 'lowentry.customizable_assets_install';


    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $bundlesWebDir;


    /** @var array */
    private $bundles = array();


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param ContainerInterface $container
     * @param string $bundlesWebDir
     */
    public function __construct(InputInterface $input, OutputInterface $output, ContainerInterface $container, $bundlesWebDir)
    {
        $this->input = $input;
        $this->output = $output;
        $this->container = $container;
        $this->bundlesWebDir = $bundlesWebDir;
    }


    /**
     * @param string $from
     * @param string $to
     */
    public function makeLink($from, $to)
    {
        $this->bundles[] = array('from' => $from, 'to' => $to);
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->bundles;
    }


    /**
     * @return InputInterface
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return string
     */
    public function getBundlesWebDir()
    {
        return $this->bundlesWebDir;
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return dirname($this->getContainer()->get('kernel')->getRootDir()) . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    public function getVendorDir()
    {
        return $this->getRootDir() . 'vendor' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function getBundleDir($name)
    {
        return $this->getContainer()->get('kernel')->getBundle($name)->getPath() . DIRECTORY_SEPARATOR;
    }
}
