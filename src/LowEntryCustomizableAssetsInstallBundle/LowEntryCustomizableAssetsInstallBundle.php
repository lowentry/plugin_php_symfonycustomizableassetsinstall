<?php

namespace LowEntryCustomizableAssetsInstallBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


class LowEntryCustomizableAssetsInstallBundle extends Bundle
{
    public function getParent()
    {
        return 'FrameworkBundle';
    }
}
